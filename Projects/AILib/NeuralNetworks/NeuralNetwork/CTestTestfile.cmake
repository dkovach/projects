# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("NeuralAlgorithms")
subdirs("NeuralNetwork")
subdirs("TestNeuralNetwork")
