# Install script for directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork/NeuralAlgorithms

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork/NeuralAlgorithms/libneural_algorithms.dll.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork/NeuralAlgorithms/cygneural_algorithms.dll")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cygneural_algorithms.dll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cygneural_algorithms.dll")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip.exe" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cygneural_algorithms.dll")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/ActivationFunctions/Source/ActivationFunctions.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Cluster/Cluster_2_0/Source/Cluster.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Fourier/Fourier/Source/Fourier.h"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/Matrix/Source/Matrix.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/Genetics_3_0/Source/Genetics.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/Genetics_3_0/Source/GeneticPair.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Metrics/Metrics/Source/Metrics.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork/NeuralNetwork/Source/NeuralCallers.h"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork/NeuralNetwork/Source/NeuralUnit.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/Random/Source/Random.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/Statistics/Source/Statistics.h"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/Utility/Utility/Source/Preprocessor.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/NeuralNetworks/NeuralNetwork/NeuralAlgorithms/Source/NeuralAlgorithmsCallers.h"
    )
endif()

