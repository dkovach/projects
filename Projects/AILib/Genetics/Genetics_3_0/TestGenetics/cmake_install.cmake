# Install script for directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/TestGenetics

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/TestGenetics/test_genetics.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/test_genetics.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/test_genetics.exe")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip.exe" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/test_genetics.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/TestGenetics/Source/CommandLineOptions.h"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/Genetics_3_0/Source/Genetics.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/Genetics_3_0/Source/GeneticPair.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/AILib/Genetics/Genetics_3_0/Genetics_3_0/Source/GeneticParameters.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/Random/Source/Random.hpp"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/Utility/Timing/Timing.h"
    "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/Utility/Utility/Source/CommandLineParser.hpp"
    )
endif()

