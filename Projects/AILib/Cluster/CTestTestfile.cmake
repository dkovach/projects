# CMake generated Testfile for 
# Source directory: /home/daniel/Hypercomplete/Projects/Projects/AILib/Cluster
# Build directory: /home/daniel/Hypercomplete/Projects/Projects/AILib/Cluster
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(Cluster_2_0)
SUBDIRS(ClusterAlgorithms)
SUBDIRS(TestCluster)
