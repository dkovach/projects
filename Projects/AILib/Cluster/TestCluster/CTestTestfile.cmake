# CMake generated Testfile for 
# Source directory: /home/daniel/Hypercomplete/Projects/Projects/AILib/Cluster/TestCluster
# Build directory: /home/daniel/Hypercomplete/Projects/Projects/AILib/Cluster/TestCluster
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(KMeansTrain "./test_cluster" "--KMeans" "--Train" "--InputFilename" "Input/input.csv")
ADD_TEST(KMeansApply "./test_cluster" "--KMeans" "--Apply" "--InputFilename" "Input/input.csv")
ADD_TEST(KMeansCrossValidate "./test_cluster" "--KMeans" "--CrossValidate" "--InputFilename" "Input/input.csv" "--XValFilename" "Input/x.csv")
ADD_TEST(KMeansGetError "./test_cluster" "--KMeans" "--GetError" "--InputFilename" "Input/input.csv")
ADD_TEST(KMeansTrainArray "./test_cluster" "--KMeans" "--Train" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(KMeansApplyArray "./test_cluster" "--KMeans" "--Apply" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(KMeansCrossValidateArray "./test_cluster" "--KMeans" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Array" "--XValFilename" "Input/x.csv")
ADD_TEST(KMeansGetErrorArray "./test_cluster" "--KMeans" "--GetError" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(KMeansTrainMatrix "./test_cluster" "--KMeans" "--Train" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(KMeansApplyMatrix "./test_cluster" "--KMeans" "--Apply" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(KMeansCrossValidateMatrix "./test_cluster" "--KMeans" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Matrix" "--XValFilename" "Input/x.csv")
ADD_TEST(KMeansGetErrorMatrix "./test_cluster" "--KMeans" "--GetError" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(KMeansTrainPreprocessor "./test_cluster" "--KMeans" "--Train" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(KMeansApplyPreprocessor "./test_cluster" "--KMeans" "--Apply" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(KMeansCrossValidatePreprocessor "./test_cluster" "--KMeans" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Preprocessor" "--XValFilename" "Input/x.csv")
ADD_TEST(KMeansGetErrorPreprocessor "./test_cluster" "--KMeans" "--GetError" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(SOMTrain "./test_cluster" "--SOM" "--Train" "--InputFilename" "Input/input.csv")
ADD_TEST(SOMApply "./test_cluster" "--SOM" "--Apply" "--InputFilename" "Input/input.csv")
ADD_TEST(SOMCrossValidate "./test_cluster" "--SOM" "--CrossValidate" "--InputFilename" "Input/input.csv" "--XValFilename" "Input/x.csv")
ADD_TEST(SOMGetError "./test_cluster" "--SOM" "--GetError" "--InputFilename" "Input/input.csv")
ADD_TEST(SOMTrainArray "./test_cluster" "--SOM" "--Train" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(SOMApplyArray "./test_cluster" "--SOM" "--Apply" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(SOMCrossValidateArray "./test_cluster" "--SOM" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Array" "--XValFilename" "Input/x.csv")
ADD_TEST(SOMGetErrorArray "./test_cluster" "--SOM" "--GetError" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(SOMTrainMatrix "./test_cluster" "--SOM" "--Train" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(SOMApplyMatrix "./test_cluster" "--SOM" "--Apply" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(SOMCrossValidateMatrix "./test_cluster" "--SOM" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Matrix" "--XValFilename" "Input/x.csv")
ADD_TEST(SOMGetErrorMatrix "./test_cluster" "--SOM" "--GetError" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(SOMTrainPreprocessor "./test_cluster" "--SOM" "--Train" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(SOMApplyPreprocessor "./test_cluster" "--SOM" "--Apply" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(SOMCrossValidatePreprocessor "./test_cluster" "--SOM" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Preprocessor" "--XValFilename" "Input/x.csv")
ADD_TEST(SOMGetErrorPreprocessor "./test_cluster" "--SOM" "--GetError" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(TopologicalTrain "./test_cluster" "--Topological" "--Train" "--InputFilename" "Input/input.csv")
ADD_TEST(TopologicalApply "./test_cluster" "--Topological" "--Apply" "--InputFilename" "Input/input.csv")
ADD_TEST(TopologicalCrossValidate "./test_cluster" "--Topological" "--CrossValidate" "--InputFilename" "Input/input.csv" "--XValFilename" "Input/x.csv")
ADD_TEST(TopologicalGetError "./test_cluster" "--Topological" "--GetError" "--InputFilename" "Input/input.csv")
ADD_TEST(TopologicalTrainArray "./test_cluster" "--Topological" "--Train" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(TopologicalApplyArray "./test_cluster" "--Topological" "--Apply" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(TopologicalCrossValidateArray "./test_cluster" "--Topological" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Array" "--XValFilename" "Input/x.csv")
ADD_TEST(TopologicalGetErrorArray "./test_cluster" "--Topological" "--GetError" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(TopologicalTrainMatrix "./test_cluster" "--Topological" "--Train" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(TopologicalApplyMatrix "./test_cluster" "--Topological" "--Apply" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(TopologicalCrossValidateMatrix "./test_cluster" "--Topological" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Matrix" "--XValFilename" "Input/x.csv")
ADD_TEST(TopologicalGetErrorMatrix "./test_cluster" "--Topological" "--GetError" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(TopologicalTrainPreprocessor "./test_cluster" "--Topological" "--Train" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(TopologicalApplyPreprocessor "./test_cluster" "--Topological" "--Apply" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(TopologicalCrossValidatePreprocessor "./test_cluster" "--Topological" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Preprocessor" "--XValFilename" "Input/x.csv")
ADD_TEST(TopologicalGetErrorPreprocessor "./test_cluster" "--Topological" "--GetError" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(SimpleTrain "./test_cluster" "--Simple" "--Train" "--InputFilename" "Input/input.csv")
ADD_TEST(SimpleApply "./test_cluster" "--Simple" "--Apply" "--InputFilename" "Input/input.csv")
ADD_TEST(SimpleCrossValidate "./test_cluster" "--Simple" "--CrossValidate" "--InputFilename" "Input/input.csv" "--XValFilename" "Input/x.csv")
ADD_TEST(SimpleGetError "./test_cluster" "--Simple" "--GetError" "--InputFilename" "Input/input.csv")
ADD_TEST(SimpleTrainArray "./test_cluster" "--Simple" "--Train" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(SimpleApplyArray "./test_cluster" "--Simple" "--Apply" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(SimpleCrossValidateArray "./test_cluster" "--Simple" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Array" "--XValFilename" "Input/x.csv")
ADD_TEST(SimpleGetErrorArray "./test_cluster" "--Simple" "--GetError" "--InputFilename" "Input/input.csv" "--Array")
ADD_TEST(SimpleTrainMatrix "./test_cluster" "--Simple" "--Train" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(SimpleApplyMatrix "./test_cluster" "--Simple" "--Apply" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(SimpleCrossValidateMatrix "./test_cluster" "--Simple" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Matrix" "--XValFilename" "Input/x.csv")
ADD_TEST(SimpleGetErrorMatrix "./test_cluster" "--Simple" "--GetError" "--InputFilename" "Input/input.csv" "--Matrix")
ADD_TEST(SimpleTrainPreprocessor "./test_cluster" "--Simple" "--Train" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(SimpleApplyPreprocessor "./test_cluster" "--Simple" "--Apply" "--InputFilename" "Input/input.csv" "--Preprocessor")
ADD_TEST(SimpleCrossValidatePreprocessor "./test_cluster" "--Simple" "--CrossValidate" "--InputFilename" "Input/input.csv" "--Preprocessor" "--XValFilename" "Input/x.csv")
ADD_TEST(SimpleGetErrorPreprocessor "./test_cluster" "--Simple" "--GetError" "--InputFilename" "Input/input.csv" "--Preprocessor")
