# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_Constructor1 "./test_myproject" "--Constructor1")
set_tests_properties(test_Constructor1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;59;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;0;")
add_test(test_Constructor2 "./test_myproject" "--Constructor2")
set_tests_properties(test_Constructor2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;60;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;0;")
add_test(test_Constructor3 "./test_myproject" "--Constructor3")
set_tests_properties(test_Constructor3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;61;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;0;")
add_test(test_Sanity "./test_myproject" "--Sanity")
set_tests_properties(test_Sanity PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;62;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;0;")
add_test(test_Test1 "./test_myproject" "--Test1")
set_tests_properties(test_Test1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;63;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MyProject/TestMyProject/CMakeLists.txt;0;")
