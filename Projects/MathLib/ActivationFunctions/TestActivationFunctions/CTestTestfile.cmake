# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_Logistic1 "./test_activation_functions" "--Logistic1")
set_tests_properties(test_Logistic1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;22;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Logistic2 "./test_activation_functions" "--Logistic2")
set_tests_properties(test_Logistic2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;23;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Logistic3 "./test_activation_functions" "--Logistic3")
set_tests_properties(test_Logistic3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;24;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Logistic4 "./test_activation_functions" "--Logistic4")
set_tests_properties(test_Logistic4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;25;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_InverseTangent1 "./test_activation_functions" "--InverseTangent1")
set_tests_properties(test_InverseTangent1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;26;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_InverseTangent2 "./test_activation_functions" "--InverseTangent2")
set_tests_properties(test_InverseTangent2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;27;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_InverseTangent3 "./test_activation_functions" "--InverseTangent3")
set_tests_properties(test_InverseTangent3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;28;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_InverseTangent4 "./test_activation_functions" "--InverseTangent4")
set_tests_properties(test_InverseTangent4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;29;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_HyperbolicTangent1 "./test_activation_functions" "--HyperbolicTangent1")
set_tests_properties(test_HyperbolicTangent1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;30;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_HyperbolicTangent2 "./test_activation_functions" "--HyperbolicTangent2")
set_tests_properties(test_HyperbolicTangent2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;31;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_HyperbolicTangent3 "./test_activation_functions" "--HyperbolicTangent3")
set_tests_properties(test_HyperbolicTangent3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;32;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_HyperbolicTangent4 "./test_activation_functions" "--HyperbolicTangent4")
set_tests_properties(test_HyperbolicTangent4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;33;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Algebraic1 "./test_activation_functions" "--Algebraic1")
set_tests_properties(test_Algebraic1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;34;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Algebraic2 "./test_activation_functions" "--Algebraic2")
set_tests_properties(test_Algebraic2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;35;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Algebraic3 "./test_activation_functions" "--Algebraic3")
set_tests_properties(test_Algebraic3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;36;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Algebraic4 "./test_activation_functions" "--Algebraic4")
set_tests_properties(test_Algebraic4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;37;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Erf1 "./test_activation_functions" "--Erf1")
set_tests_properties(test_Erf1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;38;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Erf2 "./test_activation_functions" "--Erf2")
set_tests_properties(test_Erf2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;39;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Erf3 "./test_activation_functions" "--Erf3")
set_tests_properties(test_Erf3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;40;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Erf4 "./test_activation_functions" "--Erf4")
set_tests_properties(test_Erf4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;41;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Gompertz1 "./test_activation_functions" "--Gompertz1")
set_tests_properties(test_Gompertz1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;42;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Gompertz2 "./test_activation_functions" "--Gompertz2")
set_tests_properties(test_Gompertz2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;43;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Gompertz3 "./test_activation_functions" "--Gompertz3")
set_tests_properties(test_Gompertz3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;44;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Gompertz4 "./test_activation_functions" "--Gompertz4")
set_tests_properties(test_Gompertz4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;45;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Control1 "./test_activation_functions" "--Control1")
set_tests_properties(test_Control1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;46;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Control2 "./test_activation_functions" "--Control2")
set_tests_properties(test_Control2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;47;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Control3 "./test_activation_functions" "--Control3")
set_tests_properties(test_Control3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;48;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
add_test(test_Control4 "./test_activation_functions" "--Control4")
set_tests_properties(test_Control4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;49;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeLists.txt;0;")
