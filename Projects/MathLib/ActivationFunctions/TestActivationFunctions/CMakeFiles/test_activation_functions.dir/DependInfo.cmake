# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/Source/main.cpp" "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/ActivationFunctions/TestActivationFunctions/CMakeFiles/test_activation_functions.dir/Source/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "Projects/MathLib/ActivationFunctions/ActivationFunctions/Source"
  "Projects/Utility/Timing"
  "Projects/Utility/Utility/Source"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
