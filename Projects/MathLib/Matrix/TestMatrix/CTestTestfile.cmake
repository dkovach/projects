# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(matrix_mul_const "test_matrix" "--MulConstMatrix")
set_tests_properties(matrix_mul_const PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;24;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_ostream "test_matrix" "--Ostream")
set_tests_properties(matrix_ostream PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;25;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_transpose "test_matrix" "--Transpose")
set_tests_properties(matrix_transpose PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;26;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_sig "test_matrix" "--Sigmoid")
set_tests_properties(matrix_sig PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;27;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_dsig "test_matrix" "--DSigmoid")
set_tests_properties(matrix_dsig PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;28;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_err "test_matrix" "--Err")
set_tests_properties(matrix_err PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;29;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_comp "test_matrix" "--Comp")
set_tests_properties(matrix_comp PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;30;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_kro "test_matrix" "--Kro")
set_tests_properties(matrix_kro PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;31;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_dot "test_matrix" "--Dot")
set_tests_properties(matrix_dot PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;32;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_sum "test_matrix" "--Sum")
set_tests_properties(matrix_sum PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;33;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_con_1 "test_matrix" "--Constructor1")
set_tests_properties(matrix_con_1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;34;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_con_2 "test_matrix" "--Constructor2")
set_tests_properties(matrix_con_2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;35;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_con_3 "test_matrix" "--Constructor3")
set_tests_properties(matrix_con_3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;36;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_fil_1 "test_matrix" "--Fill1")
set_tests_properties(matrix_fil_1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;37;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_fil_2 "test_matrix" "--Fill2")
set_tests_properties(matrix_fil_2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;38;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_fil_3 "test_matrix" "--Fill3")
set_tests_properties(matrix_fil_3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;39;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_put_m "test_matrix" "--PutMatrix")
set_tests_properties(matrix_put_m PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;40;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_get_m "test_matrix" "--GetMatrix")
set_tests_properties(matrix_get_m PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;41;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_get_r "test_matrix" "--GetRows")
set_tests_properties(matrix_get_r PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;42;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_get_c "test_matrix" "--GetColumns")
set_tests_properties(matrix_get_c PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;43;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_size "test_matrix" "--Size")
set_tests_properties(matrix_size PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;44;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_at "test_matrix" "--At")
set_tests_properties(matrix_at PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;45;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_add "test_matrix" "--Addition")
set_tests_properties(matrix_add PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;46;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_sub "test_matrix" "--Subtraction")
set_tests_properties(matrix_sub PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;47;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_mul "test_matrix" "--Multiplication")
set_tests_properties(matrix_mul PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;48;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_assn "test_matrix" "--Assignment")
set_tests_properties(matrix_assn PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;49;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_add_a "test_matrix" "--AdditionAssignment")
set_tests_properties(matrix_add_a PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;50;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
add_test(matrix_sub_a "test_matrix" "--SubtractionAssignment")
set_tests_properties(matrix_sub_a PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;51;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Matrix/TestMatrix/CMakeLists.txt;0;")
