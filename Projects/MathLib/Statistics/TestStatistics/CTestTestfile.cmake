# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_max "./test_statistics" "--Max" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_max PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;25;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
add_test(test_mean "./test_statistics" "--Mean" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_mean PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;26;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
add_test(test_standard_deviation "./test_statistics" "--StandardDeviation" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_standard_deviation PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;27;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
add_test(test_covariance "./test_statistics" "--Covariance" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_covariance PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;28;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
add_test(test_mean_centered_covariance "./test_statistics" "--MeanCenteredCovariance" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_mean_centered_covariance PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;29;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
add_test(test_normalize_by_column "./test_statistics" "--NormalizeByColumn" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_normalize_by_column PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;30;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
add_test(test_normalize_by_row "./test_statistics" "--NormalizeByRow" "--InputFile" "Input/input.csv" "--Tokens" ";, ")
set_tests_properties(test_normalize_by_row PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;31;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Statistics/TestStatistics/CMakeLists.txt;0;")
