# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_RandomConstructor1 "./test_random" "--RandomConstructor1")
set_tests_properties(test_RandomConstructor1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;22;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_RandomConstructor2 "./test_random" "--RandomConstructor2")
set_tests_properties(test_RandomConstructor2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;23;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_RandomConstructor3 "./test_random" "--RandomConstructor3")
set_tests_properties(test_RandomConstructor3 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;24;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_RandomConstructor4 "./test_random" "--RandomConstructor4")
set_tests_properties(test_RandomConstructor4 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;25;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_TestSetSeed "./test_random" "--TestSetSeed")
set_tests_properties(test_TestSetSeed PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;26;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_TestGetSeed "./test_random" "--TestGetSeed")
set_tests_properties(test_TestGetSeed PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;27;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_TestGenerate1 "./test_random" "--TestGenerate1")
set_tests_properties(test_TestGenerate1 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;28;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_TestGenerate2 "./test_random" "--TestGenerate2")
set_tests_properties(test_TestGenerate2 PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;29;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
add_test(test_Generate "./test_random" "--Generate")
set_tests_properties(test_Generate PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;30;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Random/TestRandom/CMakeLists.txt;0;")
