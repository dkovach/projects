# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("ActivationFunctions")
subdirs("Fourier")
subdirs("Matrix")
subdirs("Metrics")
subdirs("Random")
subdirs("Statistics")
