# CMake generated Testfile for 
# Source directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Metrics
# Build directory: /home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Metrics
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_levenshtein "python" "Levenshtein/Levenshtein.py")
set_tests_properties(test_levenshtein PROPERTIES  _BACKTRACE_TRIPLES "/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Metrics/CMakeLists.txt;7;add_test;/home/Dell/Dell/Desktop/Projects/Projects-master/Projects/MathLib/Metrics/CMakeLists.txt;0;")
subdirs("Metrics")
subdirs("TestMetrics")
